<?php
function actionActualite($twig, $db){
 $form = array();
 $actualite = new actualite($db);
 if(isset($_POST['btSupprimer'])){
 $cocher = $_POST['cocher'];
 $form['valide'] = true;
 foreach ( $cocher as $id){
 $exec=$actualite->delete($id);
 if (!$exec){
 $form['valide'] = false;
 $form['message'] = 'Problème de suppression dans la table actualite';
 }
 }
 }

 if(isset($_GET['id'])){
 $exec=$actualite->delete($_GET['id']);
 if (!$exec){
 $form['valide'] = false; 
  $form['message'] = 'Problème de suppression dans la table actualite';
 }
 else{
 $form['valide'] = true;
 $form['message'] = 'Actualite supprimé avec succès';
 }
 }

 $liste = $actualite->select();
 echo $twig->render('actualite.html.twig', array('form'=>$form,'liste'=>$liste));
}

function actionModifactualite($twig, $db){
 $form = array();
 if(isset($_GET['id'])){
 $actualite = new Actualite($db);
 $unActivite = $actualite->selectById($_GET['id']);
 if ($unActivite!=null){
 $form['activite'] = $unActivite;

 }
 else{
 $form['message'] = 'actualite incorrect';
    }
 }
 
  else{
 if(isset($_POST['btModifa'])){
 $actualite = new Actualite($db);
 $id = $_POST['id'];
 $titre = $_POST['titre'];
 $contenu = $_POST['contenu'];
 $date = $_POST['date'];
 $photo = $_POST['photo'];
 

 $exec=$actualite->update($id, $titre, $contenu, $date, $photo);
 if(!$exec){
 $form['valide'] = false;
 $form['message'] = 'Échec de la modification';
 }
 
 else{
 $form['valide'] = true;
 $form['message'] = 'Modification réussie';
 }
 }
 

}
echo $twig->render('modif_actualite.html.twig', array('form'=>$form));
}
?>