<?php
function actionUtilisateur($twig, $db){
 $form = array();
 $utilisateur = new Utilisateur($db);
 $liste = $utilisateur->select();
 echo $twig->render('utilisateur.html.twig', array('form'=>$form,'liste'=>$liste));
}
function actionModifUtilisateur($twig, $db){
 $form = array();
 if(isset($_GET['email'])){
 $utilisateur = new Utilisateur($db);
 $unUtilisateur = $utilisateur->selectByEmail($_GET['email']);
 if ($unUtilisateur!=null){
 $form['utilisateur'] = $unUtilisateur;
 $role = new Role($db);
 $liste = $role->select();
 $form['role']=$liste;

 }
 else{
 $form['message'] = 'Utilisateur incorrect';
    }
 }
 
  else{
 if(isset($_POST['btModifu'])){
 $utilisateur = new Utilisateur($db);
 $nom = $_POST['nom'];
 $prenom = $_POST['prenom'];
 $role = $_POST['role'];
 $email = $_POST['email'];
 $mdp = $_POST['password'];
 $mdp2 = $_POST['password2'];
 
 if($mdp!=$mdp2){
     $form['valide'] = false;
     $form['message'] = 'mots de passe différent';
 }
 else{

 $exec=$utilisateur->update($email, $role, $nom, $prenom, password_hash($mdp,PASSWORD_DEFAULT));
 if(!$exec){
 $form['valide'] = false;
 $form['message'] = 'Échec de la modification';
 }
 else{
 $form['valide'] = true;
 $form['message'] = 'Modification réussie';
 }
 }
 }
 else{
 $form['valide'] = false;   
 $form['message'] = 'Utilisateur non précisé';
}
 
}
echo $twig->render('modif_utilisateur.html.twig', array('form'=>$form));
}
?>
