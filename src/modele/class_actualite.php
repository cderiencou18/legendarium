<?php
class actualite{
 private $db;
 private $select;


 public function __construct($db){
  $this->db = $db ;
  $this->select = $db->prepare("select id, titre, contenu, date from actualite");
  $this->insert = $db->prepare("insert into actualite (id, titre, contenu, date, photo) values (:id, :titre, :contenu, :date :photo)");
  
}
  public function select(){
 $liste = $this->select->execute();
 if ($this->select->errorCode()!=0){
 print_r($this->select->errorInfo());
 }
 return $this->select->fetchAll();
 }

 public function insert($id, $titre, $contenu, $date, $photo){
 $r = true;
 $this->insert->execute(array(':id'=>$id, ':titre'=>$titre, ':contenu'=>$contenu, ':date'=>$date, ':photo'=>$photo,));
 if ($this->insert->errorCode()!=0){
 print_r($this->insert->errorInfo());
 $r=false;
 }
 return $r;
 }
 
}

?>

