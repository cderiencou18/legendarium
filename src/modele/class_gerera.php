<?php
class gerera{
 private $db;
 private $insert;
 private $select;

public function __construct($db){ 
  $this->db = $db ;
  $this->insert = $db->prepare("insert into actualite (id, titre, contenu, date, photo) values (:id, :titre, :contenu, :date, :photo)");
  $this->select = $db->prepare("select titre, contenu, date from actualite");

 
 }
 
 public function insert($id, $titre, $contenu, $date, $photo){
 $r = true;
 $this->insert->execute(array(':id'=>$id, ':titre'=>$titre, ':contenu'=>$contenu, ':date'=>$date, ':photo'=>$photo,));
 if ($this->insert->errorCode()!=0){
 print_r($this->insert->errorInfo());
 $r=false;
 }
 return $r;
 }
 
  public function select(){
 $liste = $this->select->execute();
 if ($this->select->errorCode()!=0){
 print_r($this->select->errorInfo());
 }
 return $this->select->fetchAll();
 }
 
}

?>
