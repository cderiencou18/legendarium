<?php
class Utilisateur{
 private $db;
 private $insert;
 private $connect;
 private $select;
 private $selectByEmail;
 private $update;


 public function __construct($db){ 
  $this->db = $db ;
  $this->insert = $db->prepare("insert into utilisateur (id, nom, prenom, email, mdp, datenaiss, idRole ) values (:id, :nom, :prenom, :email, :mdp, :datenaiss, :role )");
  $this->connect = $db->prepare("select email, idRole, mdp from utilisateur where email=:email");
  $this->select = $db->prepare("select email, idRole, nom, prenom, r.libelle as libellerole from utilisateur u, role r where u.idRole = r.id order by nom");
  $this->selectByEmail = $db->prepare("select email, nom, prenom, idRole from utilisateur where email=:email");
  $this->update = $db->prepare("update utilisateur set nom=:nom, prenom=:prenom, idRole=:role, mdp=:mdp where email=:email");
 
 }

public function insert($id, $nom, $prenom, $email, $mdp, $datenaiss, $idRole){
 $r = true;
 $this->insert->execute(array(':id'=>$id, ':nom'=>$nom, ':prenom'=>$prenom, ':email'=>$email, ':mdp'=>$mdp, ':datenaiss'=>$datenaiss, ':role'=>$idRole));
 if ($this->insert->errorCode()!=0){
 print_r($this->insert->errorInfo());
 $r=false;
 }
 return $r;
 }
public function connect($email){
 $this->connect->execute(array(':email'=>$email));
 if ($this->connect->errorCode()!=0){
 print_r($this->connect->errorInfo());
 }
 return $this->connect->fetch();
 }
  public function select(){
 $liste = $this->select->execute();
 if ($this->select->errorCode()!=0){
 print_r($this->select->errorInfo());
 }
 return $this->select->fetchAll();
 }

 public function selectByEmail($email){
 $this->selectByEmail->execute(array(':email'=>$email));
 if ($this->selectByEmail->errorCode()!=0){
 print_r($this->selectByEmail->errorInfo());
 }
 return $this->selectByEmail->fetch();
 }

 public function update($email, $role, $nom, $prenom, $mdp){
 $r = true;
 $this->update->execute(array(':email'=>$email, ':role'=>$role, ':nom'=>$nom, ':prenom'=>$prenom, ':mdp'=>$mdp));
 if ($this->update->errorCode()!=0){
 print_r($this->update->errorInfo());
 $r=false;
 }
 return $r;
 }
 
}

?>
